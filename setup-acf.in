#!/bin/sh

PROGRAM=setup-acf
VERSION=@VERSION@

PREFIX=@PREFIX@
: ${LIBDIR=$PREFIX/lib}
. "$LIBDIR/libalpine.sh"

usage() {
	echo "usage: $PROGRAM [-ahn] [-e email] [-l address] [PACKAGE...]"
	exit $1
}

pkgs="acf-core acf-alpine-baselayout acf-apk-tools openssl"

while getopts "ae:hl:n" opt ; do
	case $opt in
		a) pkgs=$(apk search --quiet --exact 'acf-*');;
		e) EMAIL="$OPTARG";;
		h) usage 0;;
		l) address="$OPTARG";;
		n) create_passwd=no;;
		'?') usage "1" >&2;;
	esac
done
shift $(expr $OPTIND - 1)

while [ $# -gt 0 ]; do
	pkgs="$pkgs acf-$1"
	shift
done

# install packages
while getopts "hc:" opt; do
	case $opt in
		c) resp="$OPTARG";;
		h) usage 0;;
		'?') usage 1;;
	esac
done
shift $(( $OPTIND - 1 ))

: ${resp:=$1}

while [ $# -eq 0 ] && ! isin "$resp" lighttpd mini_httpd; do
	ask "Which ACF server to run? ('lighttpd', 'mini_httpd')" lighttpd
done

serv="$resp"

apk add $serv $pkgs || exit 1

if [ "$create_passwd" != "no" ]; then
	mkdir -p /etc/acf
	if [ -f /etc/acf/passwd ]; then
		mv /etc/acf/passwd /etc/acf/passwd.backup
	fi
	echo "root:x:Admin account:ADMIN" >/etc/acf/passwd
	chmod 600 /etc/acf/passwd
	acfpasswd -s root
fi

# setup ACF server and start it
if [ -d /var/www/localhost/htdocs ]; then
	mv /var/www/localhost/htdocs /var/www/localhost/htdocs.old
fi
# link ACF root folder
mkdir -p /var/www/localhost/
ln -s /usr/share/acf/www/ /var/www/localhost/htdocs
lbu add /var/www/localhost/htdocs
# ssl env
SSLDIR=/etc/ssl/$serv
SSLCNF=$SSLDIR/$serv.cnf
KEYFILE=$SSLDIR/server.key
CRTFILE=$SSLDIR/server.crt
PEMFILE=$SSLDIR/server.pem

if [ -f $PEMFILE ]; then
	echo "$PEMFILE already exist."
else
	mkdir -p $SSLDIR
	cat >$SSLCNF <<-__EOF__
		[ req ]
		default_bits = 1024
		encrypt_key = yes
		distinguished_name = req_dn
		x509_extensions = cert_type
		prompt = no

		[ req_dn ]
		OU=HTTPS server
		CN=$(hostname -f || hostname)
		emailAddress=${EMAIL:-postmaster@example.com}

		[ cert_type ]
		nsCertType = server
	__EOF__
	echo "Generating certificates for HTTPS..."
	openssl genrsa 2048 > $KEYFILE
	openssl req -new -x509 -nodes -sha1 -days 3650 -key $KEYFILE \
		-config $SSLCNF > $CRTFILE
	cat $KEYFILE >> $CRTFILE
	rm $KEYFILE
	mv $CRTFILE $PEMFILE
fi

if  [ "$serv" = "lighttpd" ]; then
	mkdir -p /etc/lighttpd/cache/acf/
	chown -R lighttpd:lighttpd /etc/lighttpd/cache/acf/
	cat >/etc/lighttpd/lighttpd.conf <<-'EOF'
		##########  ACF LIGHTTPD CONF  ##########
		# {{{ variables
		var.basedir  = "/var/www/localhost"
		var.logdir   = "/var/log/lighttpd"
		var.statedir = "/var/lib/lighttpd"
		# }}}
		# {{{ modules
		# At the very least, mod_access and mod_accesslog should be enabled.
		# All other modules should only be loaded if necessary.
		# NOTE: the order of modules is important.
		server.modules = (
		"mod_openssl",
		"mod_access",
		"mod_setenv",
		"mod_deflate",
		"mod_expire",
		"mod_accesslog"
		)
		# }}}
		# {{{ includes
		include "mime-types.conf"
		include "mod_cgi.conf"
		# }}}
		# {{{ server settings
		server.username      = "lighttpd"
		server.groupname     = "lighttpd"
		server.document-root = var.basedir + "/htdocs"
		server.pid-file      = "/run/lighttpd.pid"
		server.errorlog      = var.logdir  + "/error.log"
		index-file.names     = ("index.html")
		# Hide server name & version
		server.tag           = ""
		# Serve HTTPS only
		server.port          = 443
		# which extensions should not be handled via static-file transfer
		# (extensions that are usually handled by mod_cgi, mod_fastcgi, etc).
		static-file.exclude-extensions = (".php", ".pl", ".cgi", ".fcgi")
		# }}}
		# {{{ mod_accesslog
		accesslog.filename   = var.logdir + "/access.log"
		# }}}
		# {{{ mod_access
		url.access-deny = ("~", ".inc")
		# }}}
		# {{{ mod_ssl
		ssl.engine    = "enable"
		ssl.pemfile   = "/etc/ssl/lighttpd/server.pem"
		# }}}
		# {{{ mod_deflate
		deflate.cache-dir   = "/etc/lighttpd/cache/acf/"
		deflate.mimetypes = (
		"text/html,
		text/css,
		text/plain,
		text/xml,
		text/x-component,
		text/javascript,
		application/x-javascript,
		application/javascript,
		application/json,
		application/manifest+json,
		application/vnd.api+json,
		application/xml,
		application/xhtml+xml,
		application/rss+xml,
		application/atom+xml,
		application/vnd.ms-fontobject,
		application/x-font-ttf,
		application/x-font-opentype,
		application/x-font-truetype,
		image/svg+xml,
		image/x-icon,
		image/vnd.microsoft.icon,
		font/ttf,
		font/eot,
		font/otf,
		font/opentype"
		)
		deflate.allowed-encodings = ( "bzip2", "gzip", "deflate" ) # "bzip2" and "zstd" also supported
		# }}}
		# {{{ extra rules
		# set Content-Encoding and reset Content-Type for browsers that
		# support decompressing on-thy-fly (requires mod_setenv)
		$HTTP["url"] =~ "\.gz$" {
		  setenv.add-response-header = ("Content-Encoding" => "x-gzip")
		  mimetype.assign = (".gz" => "text/plain")
		}
		$HTTP["url"] =~ "\.bz2$" {
		  setenv.add-response-header = ("Content-Encoding" => "x-bzip2")
		  mimetype.assign = (".bz2" => "text/plain")
		}
		# Cache based on suffix
		$HTTP["url"] =~ "\.(ico|jpg|png|css|js|html)$" {
		  expire.url = ( "" => "access plus 1 years" )
		}
		##########  SECURE ACCESS FROM LAN ##########
		# Allow only LAN IP
		#$HTTP["remoteip"] != "192.168.1.0/24" {
		#  url.access-deny = ("")
		#}
		#############################################
		# }}}
	EOF
	# enable build-in cgi module rewrite
	cat >/etc/lighttpd/mod_cgi.conf <<-'EOF'
		#
		# see cgi.txt for more information on using mod_cgi
		#
		server.modules += ("mod_cgi")

		$HTTP["url"] =~ "^/cgi-bin/" {
		# only allow cgi's in this directory
		cgi.assign = ( "" => "" )
		}
	EOF
	echo -e "ACF \e[1mlighttpd\e[0m server \e[1minstalled\e[0m"
fi

if  [ "$serv" = "mini_httpd" ]; then
cat >/etc/mini_httpd/mini_httpd.conf <<-__EOF__
	##########  ACF MINI_HTTPD CONF  ##########
	nochroot
	dir=/var/www/localhost/htdocs
	user=nobody
	cgipat=cgi-bin**
	certfile=$PEMFILE
	port=443
	ssl
__EOF__
  if [ -n "$address" ]; then
    echo "host=$address" >> /etc/mini_httpd/mini_httpd.conf
  fi
  echo -e "ACF \e[1mmini_httpd\e[0m server \e[1minstalled\e[0m"
fi

# Stop services if started
rc-service -s lighttpd stop
rc-service -s mini_httpd stop
# start ACF server
rc-update -q add $serv default
rc-service $serv start
# remember for user to use ssl & protect from WAN
HOST=$(ip route get 1 | sed 's/^.*src \([^ ]*\).*$/\1/;q')
echo -e "Visit \e[1m\e[4mhttps://$HOST\e[0m - \e[1mBe careful if you allow access through WAN\e[0m"
# force update of dependency cache
rc-update -q --update
